import React, { Component } from 'react'
import './App.scss'
import Carte from './Cartes/Carte'
import Menu from './menu/menu'

// on export le component et definit la classe en même temps
export default class App extends Component {
  constructor (props) {
    super(props)
    this.state = {
      id: '',
    }
  }

  // on récupère la valeur de l'enfant et on stoque cette donnée dans le states local
  callbackFunction (value) {
    this.setState({
      id: value
    })
  }

  render () {
    return (
      <div className="conteneur">
        {/* on appelle le component que l'on souhaite et on lui indique qu'il va recevoir une donnée */}
        <Menu callbackFromParent = {this.callbackFunction.bind(this)}/>

        {/* on appele le component et on lui transmet une donnée qu'il doit conserver */}
        <Carte dataFromParent = {this.state.id}/>
      </div>
    )
  }
}
