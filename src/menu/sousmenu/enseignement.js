import React, { Component } from 'react'
import Card from 'react-bootstrap/Card'
import Accordion from 'react-bootstrap/Accordion'
import './menu.scss'

import enseignement from '../../img/enseignement.png'
import descendre from '../../img/descendre.png'

class Enseignement extends Component {
  // on récupère la valeur de la checkbox et la valeur de l'id, puis on dmet une condition sur la valeur du check selon laquel
  // on dit que si la valeur du check est true d'envoyer au parent la valeur de l'id et sinon si c'est false
  // d'envoyer la valeur du check :"false"
  handleInputChange= event => {
    let id = event.target.value
    let check = event.target.checked
    if (check === true) {
      this.props.callbackFromParent(id)
    } else {
      this.props.callbackFromParent(check)
    }
  }

  render () {
    // afficher l'accordéon bootstrap
    return (
      <Card>
        <Accordion.Toggle as={Card.Header} eventKey="5">
          <span>
            <img src={enseignement} alt="enseignement"></img>
        ENSEIGNEMENT
            <img src={descendre} onClick={this.onClick} alt="descendre"></img>
          </span>
        </Accordion.Toggle>
        <Accordion.Collapse eventKey="5">
          <Card.Body>
            <ul>
              {/* pour chaque items on définit une checkbox qui sera caché par un "faux" bouton ce  qui permet qu'au moment du click (checkbox) l'estétique du "bouton" change et enclenche une methode */}
              <li><label htmlFor="24"><input type="checkbox" id="24" value ="24" name="interest" onChange={this.handleInputChange}></input><span className="check"></span> Ecoles primaires</label> </li>
              <li><label htmlFor="25"><input type="checkbox" id="25" value ="25" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Collèges</label> </li>
              <li><label htmlFor="26"><input type="checkbox" id="26" value ="26" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Collèges-zones de rattachement</label> </li>
              <li><label htmlFor="27"><input type="checkbox" id="27" value ="27" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Lycées</label> </li>
              <li><label htmlFor="28"><input type="checkbox" id="28" value ="28" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Enseignement supérieurs</label> </li>
            </ul></Card.Body>
        </Accordion.Collapse>
      </Card>

    )
  }
}

export default Enseignement
