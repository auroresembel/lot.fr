// on importe les éléments dont on a besoin pour faire fonctionner la page
import React, { Component } from 'react'
import Card from 'react-bootstrap/Card'
import Accordion from 'react-bootstrap/Accordion'
import './menu.scss'
// on importe les images que l'on veux utiliser via le chemin de l'image
import sante from '../../img/santé.png'
import descendre from '../../img/descendre.png'

class SanteSocial extends Component {
  // on récupère la valeur de la checkbox et la valeur de l'id, puis on dmet une condition sur la valeur du check selon laquel
  // on dit que si la valeur du check est true d'envoyer au parent la valeur de l'id et sinon si c'est false
  // d'envoyer la valeur du check :"false"
  handleInputChange= event => {
    let id = event.target.value
    let check = event.target.checked
    if (check === true) {
      this.props.callbackFromParent(id)
    } else {
      this.props.callbackFromParent(check)
    }
  }

  render () {
    // afficher l'accordéon bootstrap
    return (
      <Card>
        <Accordion.Toggle as={Card.Header} eventKey="1">
          <span >
            <img src={sante} alt="santé/social"></img>
        SANTE/SOCIAL
            <img src={descendre} alt="descendre"></img>
          </span>
        </Accordion.Toggle>
        <Accordion.Collapse eventKey="1">
          <Card.Body as={Card.Body}>
            <ul>
              {/* pour chaque items on définit une checkbox qui sera caché par un "faux" bouton ce  qui permet qu'au moment du click (checkbox) l'estétique du "bouton" change et enclenche une methode */}
              <li><label htmlFor="1"><input type="checkbox" id="1" value ="1" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Maison des Solidarités départementales</label></li>
              <li><label htmlFor="2"><input type="checkbox" id="2" value ="2" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Maison des services au public (MSAP)</label></li>
              <li><label htmlFor="3"><input type="checkbox" id="3" value ="3" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Maison de santé</label></li>
              <li><label htmlFor="4"><input type="checkbox" id="4" value ="4" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Centre de plannification</label></li>
              <li><label htmlFor="5"><input type="checkbox" id="5" value ="5" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Hopitaux / cliniques</label></li>
              <li><label htmlFor="6"><input type="checkbox" id="6" value ="6" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Pharmacies</label></li>
              <li><label htmlFor="7"><input type="checkbox" id="7" value ="7" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Laboratoires d\'analyses médicales</label></li>
              <li><label htmlFor="8"><input type="checkbox" id="8" value ="8" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Radiologie</label></li>
              <li><label htmlFor="9"><input type="checkbox" id="9" value ="9" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>CPAM</label></li>
              <li><label htmlFor="10"><input type="checkbox" id="10" value ="10" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Mutualité sociale agricole</label></li>

            </ul>
          </Card.Body>
        </Accordion.Collapse>
      </Card>
    )
  }
}

// exporter pour mieux importer!!!
export default SanteSocial
