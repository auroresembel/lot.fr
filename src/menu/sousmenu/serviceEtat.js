import React, { Component } from 'react'
import Card from 'react-bootstrap/Card'
import Accordion from 'react-bootstrap/Accordion'
import './menu.scss'

import service from '../../img/service.png'
import descendre from '../../img/descendre.png'

class ServiceEtat extends Component {
  // on récupère la valeur de la checkbox et la valeur de l'id, puis on dmet une condition sur la valeur du check selon laquel
  // on dit que si la valeur du check est true d'envoyer au parent la valeur de l'id et sinon si c'est false
  // d'envoyer la valeur du check :"false"
  handleInputChange= event => {
    let id = event.target.value
    let check = event.target.checked
    if (check === true) {
      this.props.callbackFromParent(id)
    } else {
      this.props.callbackFromParent(check)
    }
  }

  render () {
    // afficher l'accordéon bootstrap
    return (
      <Card>
        <Accordion.Toggle as={Card.Header} eventKey="13">
          <span>
            <img src={service} alt="service de l'état"></img>
          SERVICE DE L'ETAT
            <img src={descendre} onClick={this.onClick} alt="descendre"></img>
          </span>
        </Accordion.Toggle>
        <Accordion.Collapse eventKey="13">
          <Card.Body>
            <ul>
              {/* pour chaque items on définit une checkbox qui sera caché par un "faux" bouton ce  qui permet qu'au moment du click (checkbox) l'estétique du "bouton" change et enclenche une methode */}
              <li><label htmlFor="59"><input type="checkbox" id="59" value ="59" name="interest" onChange={this.handleInputChange}></input><span className="check"></span> Trésor Public</label> </li>
              <li><label htmlFor="60"><input type="checkbox" id="60" value ="60" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Préfecture</label> </li>
            </ul>
          </Card.Body>
        </Accordion.Collapse>
      </Card>

    )
  }
}

export default ServiceEtat
