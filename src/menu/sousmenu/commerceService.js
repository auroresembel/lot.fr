import React, { Component } from 'react'
import Card from 'react-bootstrap/Card'
import Accordion from 'react-bootstrap/Accordion'
import './menu.scss'

import commerce from '../../img/commerce.png'
import descendre from '../../img/descendre.png'

class CommerceService extends Component {
  // on récupère la valeur de la checkbox et la valeur de l'id, puis on dmet une condition sur la valeur du check selon laquel
  // on dit que si la valeur du check est true d'envoyer au parent la valeur de l'id et sinon si c'est false
  // d'envoyer la valeur du check :"false"
 handleInputChange= event => {
   let id = event.target.value
   let check = event.target.checked
   if (check === true) {
     this.props.callbackFromParent(id)
   } else {
     this.props.callbackFromParent(check)
   }
 }

 render () {
   // afficher l'accordéon bootstrap
   return (
     <Card>
       <Accordion.Toggle as={Card.Header} eventKey="7">
         <span>
           <img src={commerce} alt="commerce/service de proximité"></img>
          COMMERCE/SERVICE DE PROXIMITE
           <img src={descendre} onClick={this.onClick} alt="descendre"></img>
         </span>
       </Accordion.Toggle>
       <Accordion.Collapse eventKey="7">
         <Card.Body>
           <ul>
             {/* pour chaque items on définit une checkbox qui sera caché par un "faux" bouton ce  qui permet qu'au moment du click (checkbox) l'estétique du "bouton" change et enclenche une methode */}
             <li><label htmlFor="32"><input type="checkbox" id="32" value ="32" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Supermarchés, supérettes</label> </li>
             <li><label htmlFor="33"><input type="checkbox" id="33" value ="33" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Boulangeries, patisseries</label> </li>
             <li><label htmlFor="34"><input type="checkbox" id="34" value ="34" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Boucherie, charcuteris</label> </li>
             <li><label htmlFor="35"><input type="checkbox" id="35" value ="35" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Agences / bureaux de poste</label> </li>
           </ul></Card.Body>
       </Accordion.Collapse>
     </Card>

   )
 }
}

export default CommerceService
