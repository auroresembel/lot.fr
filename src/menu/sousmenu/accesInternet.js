import React, { Component } from 'react'
import Card from 'react-bootstrap/Card'
import Accordion from 'react-bootstrap/Accordion'
import './menu.scss'

import internet from '../../img/internet.png'
import descendre from '../../img/descendre.png'

class AccesInternet extends Component {
  // on récupère la valeur de la checkbox et la valeur de l'id, puis on dmet une condition sur la valeur du check selon laquel
  // on dit que si la valeur du check est true d'envoyer au parent la valeur de l'id et sinon si c'est false
  // d'envoyer la valeur du check :"false"
 handleInputChange= event => {
   let id = event.target.value
   let check = event.target.checked
   if (check === true) {
     this.props.callbackFromParent(id)
   } else {
     this.props.callbackFromParent(check)
   }
 }

 render () {
   // afficher l'accordéon bootstrap
   return (
     <Card>
       <Accordion.Toggle as={Card.Header} eventKey="9">
         <span>
           <img src={internet} alt="accés internet"></img>
        ACCES INTERNET
           <img src={descendre} onClick={this.onClick} alt="descendre"></img>
         </span>
       </Accordion.Toggle>
       <Accordion.Collapse eventKey="9">
         <Card.Body>
           <ul>
             {/* pour chaque items on définit une checkbox qui sera caché par un "faux" bouton ce  qui permet qu'au moment du click (checkbox) l'estétique du "bouton" change et enclenche une methode */}
             <li><label htmlFor="45"><input type="checkbox" id="45" value ="45" name="interest" onChange={this.handleInputChange}></input><span className="check"></span> Espace publics multimedia</label> </li>
             <li><label htmlFor="46"><input type="checkbox" id="46" value ="46" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Maison de services aux publics (MSAP)</label> </li>
             <li><label htmlFor="47"><input type="checkbox" id="47" value ="47" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Centres de télétravail / Coworking</label> </li>
             <li><label htmlFor="48"><input type="checkbox" id="48" value ="48" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Fab Labs</label> </li>
             <li><label htmlFor="49"><input type="checkbox" id="49" value ="49" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Déploiement trés haut débit</label> </li>
             <li><label htmlFor="50"><input type="checkbox" id="50" value ="50" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Déploiement haut débit</label> </li>
             <li><label htmlFor="51"><input type="checkbox" id="51" value ="51" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Bornes Wifi</label> </li>
           </ul></Card.Body>
       </Accordion.Collapse>
     </Card>

   )
 }
}

export default AccesInternet
