import React, { Component } from 'react'
import Card from 'react-bootstrap/Card'
import Accordion from 'react-bootstrap/Accordion'
import './menu.scss'

import handicap from '../../img/handicap.png'
import descendre from '../../img/descendre.png'

class Handicap extends Component {
  // on récupère la valeur de la checkbox et la valeur de l'id, puis on dmet une condition sur la valeur du check selon laquel
  // on dit que si la valeur du check est true d'envoyer au parent la valeur de l'id et sinon si c'est false
  // d'envoyer la valeur du check :"false"
  handleInputChange= event => {
    let id = event.target.value
    let check = event.target.checked
    if (check === true) {
      this.props.callbackFromParent(id)
    } else {
      this.props.callbackFromParent(check)
    }
  }

  render () {
    // afficher l'accordéon bootstrap
    return (
      <Card>
        <Accordion.Toggle as={Card.Header} eventKey="4">
          <span>
            <img src={handicap} alt="handicap"></img>
          HANDICAP
            <img src={descendre} onClick={this.onClick} alt="descendre"></img>
          </span>
        </Accordion.Toggle>
        <Accordion.Collapse eventKey="4">
          <Card.Body>
            <ul>
              {/* pour chaque items on définit une checkbox qui sera caché par un "faux" bouton ce  qui permet qu'au moment du click (checkbox) l'estétique du "bouton" change et enclenche une methode */}
              <li><label htmlFor="20"><input type="checkbox" id="20" value ="20" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Information et accompagnement</label> </li>
              <li><label htmlFor="21"><input type="checkbox" id="21" value ="21" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Hebergements</label> </li>
              <li><label htmlFor="22"><input type="checkbox" id="22" value ="22" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Centres d'accueil de jour médicalisés</label> </li>
              <li><label htmlFor="23"><input type="checkbox" id="23" value ="23" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Services d'aide à domicile</label> </li>
            </ul> </Card.Body>
        </Accordion.Collapse>
      </Card>

    )
  }
}

export default Handicap
