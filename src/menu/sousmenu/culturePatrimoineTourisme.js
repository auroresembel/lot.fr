import React, { Component } from 'react'
import Card from 'react-bootstrap/Card'
import Accordion from 'react-bootstrap/Accordion'
import './menu.scss'

import culture from '../../img/culture.png'
import descendre from '../../img/descendre.png'

class CulturePatrimoineTourisme extends Component {
  // on récupère la valeur de la checkbox et la valeur de l'id, puis on dmet une condition sur la valeur du check selon laquel
  // on dit que si la valeur du check est true d'envoyer au parent la valeur de l'id et sinon si c'est false
  // d'envoyer la valeur du check :"false"

  handleInputChange= event => {
    let id = event.target.value
    let check = event.target.checked
    if (check === true) {
      this.props.callbackFromParent(id)
    } else {
      this.props.callbackFromParent(check)
    }
  }

  render () {
    // afficher l'accordéon bootstrap
    return (
      <Card>
        <Accordion.Toggle as={Card.Header} eventKey="8">
          <span>
            <img src={culture} alt="culture/patrimoine/tourisme"></img>
          CULTURE/PATRIMOINE/TOURISME
            <img src={descendre} onClick={this.onClick} alt="descendre"></img>
          </span>
        </Accordion.Toggle>
        <Accordion.Collapse eventKey="8">
          <Card.Body>
            <ul>
              {/* pour chaque items on définit une checkbox qui sera caché par un "faux" bouton ce  qui permet qu'au moment du click (checkbox) l'estétique du "bouton" change et enclenche une methode */}
              <li><label htmlFor="36"><input type="checkbox" id="36" value ="36" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Musées / centre d'art</label> </li>
              <li><label htmlFor="37"><input type="checkbox" id="37" value ="37" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Salle de spectacles</label> </li>
              <li><label htmlFor="38"><input type="checkbox" id="38" value ="38" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Bibliothèques</label> </li>
              <li><label htmlFor="39"><input type="checkbox" id="39" value ="39" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Enseignement artistique</label> </li>
              <li><label htmlFor="40"><input type="checkbox" id="40" value ="40" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Artothèque</label> </li>
              <li><label htmlFor="41"><input type="checkbox" id="41" value ="41" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Sites patrimoniaux</label> </li>
              <li><label htmlFor="42"><input type="checkbox" id="42" value ="42" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Circuits espaces naturels sensibles (ENS)</label> </li>
              <li><label htmlFor="43"><input type="checkbox" id="43" value ="43" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Archives départementales</label> </li>
              <li><label htmlFor="44"><input type="checkbox" id="44" value ="44" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Office de trourisme</label> </li>
            </ul> </Card.Body>
        </Accordion.Collapse>
      </Card>

    )
  }
}

export default CulturePatrimoineTourisme
