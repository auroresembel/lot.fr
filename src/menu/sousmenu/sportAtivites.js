import React, { Component } from 'react'
import Card from 'react-bootstrap/Card'
import Accordion from 'react-bootstrap/Accordion'
import './menu.scss'

import sport from '../../img/sport.png'
import descendre from '../../img/descendre.png'

class SportActivites extends Component {
  // on récupère la valeur de la checkbox et la valeur de l'id, puis on dmet une condition sur la valeur du check selon laquel
  // on dit que si la valeur du check est true d'envoyer au parent la valeur de l'id et sinon si c'est false
  // d'envoyer la valeur du check :"false"
handleInputChange= event => {
  let id = event.target.value
  let check = event.target.checked
  if (check === true) {
    this.props.callbackFromParent(id)
  } else {
    this.props.callbackFromParent(check)
  }
}

render () {
  // afficher l'accordéon bootstrap
  return (
    <Card>
      <Accordion.Toggle as={Card.Header} eventKey="15">
        <span>
          <img src={sport} alt="sport/activités de pleine nature"></img>
    SPORT/ ACTIVITES DE PLEINE NATURE
          <img src={descendre} onClick={this.onClick} alt="descendre"></img>
        </span>
      </Accordion.Toggle>
      <Accordion.Collapse eventKey="15">
        <Card.Body>
          <ul>
            {/* pour chaque items on définit une checkbox qui sera caché par un "faux" bouton ce  qui permet qu'au moment du click (checkbox) l'estétique du "bouton" change et enclenche une methode */}
            <li><label htmlFor="63"><input type="checkbox" id="63" value ="63" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Comités sportifs</label> </li>
            <li><label htmlFor="64"><input type="checkbox" id="64" value ="64" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Baignades surveillées, aménagées</label> </li>
            <li><label htmlFor="65"><input type="checkbox" id="65" value ="65" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Circuits espaces naturels sensibles (ENS)</label> </li>
            <li><label htmlFor="66"><input type="checkbox" id="66" value ="66" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Tourisme fluvial</label> </li>
          </ul></Card.Body>
      </Accordion.Collapse>
    </Card>

  )
}
}

export default SportActivites
