import React, { Component } from 'react'
import Card from 'react-bootstrap/Card'
import Accordion from 'react-bootstrap/Accordion'
import './menu.scss'

import senior from '../../img/senior.png'
import descendre from '../../img/descendre.png'

class Senior extends Component {
  // on récupère la valeur de la checkbox et la valeur de l'id, puis on dmet une condition sur la valeur du check selon laquel
  // on dit que si la valeur du check est true d'envoyer au parent la valeur de l'id et sinon si c'est false
  // d'envoyer la valeur du check :"false"
  handleInputChange= event => {
    let id = event.target.value
    let check = event.target.checked
    if (check === true) {
      this.props.callbackFromParent(id)
    } else {
      this.props.callbackFromParent(check)
    }
  }

  render () {
    // afficher l'accordéon bootstrap
    return (
      <Card>
        <Accordion.Toggle as={Card.Header} eventKey="3">
          <span>
            <img src={senior} alt="sénior"></img>
        SENIORS
            <img src={descendre} alt="descendre"></img>
          </span>
        </Accordion.Toggle>
        <Accordion.Collapse eventKey="3">
          <Card.Body>
            <ul>
              {/* pour chaque items on définit une checkbox qui sera caché par un "faux" bouton ce  qui permet qu'au moment du click (checkbox) l'estétique du "bouton" change et enclenche une methode */}
              <li><label htmlFor="17"><input type="checkbox" id="17" value ="17" name="interest" onChange={this.handleInputChange}></input><span className="check"></span> Espace personnes âgées</label> </li>
              <li><label htmlFor="18"><input type="checkbox" id="20" value ="18" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Hebergements</label></li>
              <li><label htmlFor="19"><input type="checkbox" id="21" value ="19" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Service d'aide à domicile</label></li>
            </ul>
          </Card.Body>
        </Accordion.Collapse>
      </Card>

    )
  }
}

export default Senior
