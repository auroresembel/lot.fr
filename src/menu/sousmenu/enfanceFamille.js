import React, { Component } from 'react'
import Card from 'react-bootstrap/Card'
import Accordion from 'react-bootstrap/Accordion'
import './menu.scss'

import enfance from '../../img/enfance.png'
import descendre from '../../img/descendre.png'

class EnfanceFamille extends Component {
  // on récupère la valeur de la checkbox et la valeur de l'id, puis on dmet une condition sur la valeur du check selon laquel
  // on dit que si la valeur du check est true d'envoyer au parent la valeur de l'id et sinon si c'est false
  // d'envoyer la valeur du check :"false"
   handleInputChange= event => {
     let id = event.target.value
     let check = event.target.checked
     if (check === true) {
       this.props.callbackFromParent(id)
     } else {
       this.props.callbackFromParent(check)
     }
   }
   render () {
     // afficher l'accordéon bootstrap
     return (
       <Card>
         <Accordion.Toggle as={Card.Header} eventKey="2">
           <span>
             <img src={enfance} alt="enfance/famille"></img>
        ENFANCE/FAMILLE
             <img src={descendre} alt="descendre"></img>
           </span>
         </Accordion.Toggle>
         <Accordion.Collapse eventKey="2">
           <Card.Body>
             <ul>
               {/* pour chaque items on définit une checkbox qui sera caché par un "faux" bouton ce  qui permet qu'au moment du click (checkbox) l'estétique du "bouton" change et enclenche une methode */}
               <li><label htmlFor="11"><input type="checkbox" id="11" value ="11" name="interest" onChange={this.handleInputChange}></input><span className="check"></span> Maison des Solidarités départementales </label> </li>
               <li><label htmlFor="12"><input type="checkbox" id="12" value ="12" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Consultations PMI</label> </li>
               <li><label htmlFor="13"><input type="checkbox" id="13" value ="13" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Crèches / haltes garderies</label> </li>
               <li><label htmlFor="14"><input type="checkbox" id="14" value ="14" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Assistante maternelles</label> </li>
               <li><label htmlFor="15"><input type="checkbox" id="15" value ="15" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Caisse d'allocations familliales-CAF</label> </li>
               <li><label htmlFor="16"><input type="checkbox" id="16" value ="16" name="interest" onChange={this.handleInputChange}></input><span className="check"></span>Service de protection de l'enfance</label> </li>
             </ul>
           </Card.Body>
         </Accordion.Collapse>
       </Card>

     )
   }
}

export default EnfanceFamille
