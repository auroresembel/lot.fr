// on import les éléments dont on a besoin pour faire fonctionner la page
import React, { Component } from 'react'
import './sousmenu/menu.scss'
import Accordion from 'react-bootstrap/Accordion'
// on importe les pages "component" que l'on veux afficher à partir de ce component
import SanteSocial from './sousmenu/santeSocial'
import EnfanceFamille from './sousmenu/enfanceFamille'
import Senior from './sousmenu/senior'
import Handicap from './sousmenu/handicap'
import Enseignement from './sousmenu/enseignement'
import EmploiEconomie from './sousmenu/emploiEconomie'
import CommerceService from './sousmenu/commerceService'
import CulturePatrimoineTourisme from './sousmenu/culturePatrimoineTourisme'
import AccesInternet from './sousmenu/accesInternet'
import Transport from './sousmenu/transport'
import Securite from './sousmenu/securite'
import Environnement from './sousmenu/environnement'
import ServiceEtat from './sousmenu/serviceEtat'
import DecoupageAdministratif from './sousmenu/decoupageAdministratif'
import SportActivites from './sousmenu/sportAtivites'

class Menu extends Component {
  // on récupère la valeur de l'enfant et on la transmet ensuite au parent au dessus
  callbackFunction=value => {
    console.log(value)
    this.props.callbackFromParent(value)
  }

  render () {
    // on affiche un accordéon de menue et sous-menues grace a bootstrap
    return (

      <div className="menue">
        <Accordion defaultActiveKey="0" className="classique">
          {/* on appelle le component que l'on souhaite et on lui indique qu'il va recevoir une donnée */}
          <SanteSocial callbackFromParent = {this.callbackFunction}/>
          <EnfanceFamille callbackFromParent = {this.callbackFunction}/>
          <Senior callbackFromParent = {this.callbackFunction}/>
          <Handicap callbackFromParent = {this.callbackFunction}/>
          <Enseignement callbackFromParent = {this.callbackFunction}/>
          <EmploiEconomie callbackFromParent = {this.callbackFunction}/>
          <CommerceService callbackFromParent = {this.callbackFunction}/>
          <CulturePatrimoineTourisme callbackFromParent = {this.callbackFunction}/>
          <AccesInternet callbackFromParent = {this.callbackFunction}/>
          <Transport callbackFromParent = {this.callbackFunction}/>
          <Securite callbackFromParent = {this.callbackFunction}/>
          <Environnement callbackFromParent = {this.callbackFunction}/>
          <ServiceEtat callbackFromParent = {this.callbackFunction}/>
          <DecoupageAdministratif callbackFromParent = {this.callbackFunction}/>
          <SportActivites callbackFromParent = {this.callbackFunction}/>
        </Accordion>

        {/* on definit le menu burger et on lui indique les éléments qu'il doit contenir (là, la même chose que le menu classique) */}
        <nav className="navbar navbar-light light-blue lighten-4" className="burger">
          <button className="navbar-toggler toggler-example" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1"
            aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation"><span className="dark-teal-text"><i
              className="fas fa-bars fa-1x"></i></span></button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent1">
            <Accordion defaultActiveKey="0" className="burgerAccordeon">
              {/* on appelle le component que l'on souhaite et on lui indique qu'il va recevoir une donnée */}
              <SanteSocial callbackFromParent = {this.callbackFunction}/>
              <EnfanceFamille callbackFromParent = {this.callbackFunction}/>
              <Senior callbackFromParent = {this.callbackFunction}/>
              <Handicap callbackFromParent = {this.callbackFunction}/>
              <Enseignement callbackFromParent = {this.callbackFunction}/>
              <EmploiEconomie callbackFromParent = {this.callbackFunction}/>
              <CommerceService callbackFromParent = {this.callbackFunction}/>
              <CulturePatrimoineTourisme callbackFromParent = {this.callbackFunction}/>
              <AccesInternet callbackFromParent = {this.callbackFunction}/>
              <Transport callbackFromParent = {this.callbackFunction}/>
              <Securite callbackFromParent = {this.callbackFunction}/>
              <Environnement callbackFromParent = {this.callbackFunction}/>
              <ServiceEtat callbackFromParent = {this.callbackFunction}/>
              <DecoupageAdministratif callbackFromParent = {this.callbackFunction}/>
              <SportActivites callbackFromParent = {this.callbackFunction}/>
            </Accordion>
          </div>
        </nav>
      </div>
    )
  }
}

export default Menu
