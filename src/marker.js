
let markers = [

  // Santé/Social
  {
    'id': '1',
    'titre': 'maison des solidarités départementales',
    'url': 'https://api-sig.lot.fr/services/GP_Sante/MapServer/0',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560328782-marker-sante.png',
    'type': 'sante',
    'where': 'CMS<> ""',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{NOM_DU_SIT}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{ADRESSE}</td></tr><tr><td> </td><td>{CODE_POSTA} {VILLE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{TELMAIL}</td></tr><td class="icon"><i class="fa fa-info"></i></td><td class="info">{INFO_WEB}</td></table>'
  },
  {
    'id': '2',
    'titre': 'maison de services au public',
    'url': 'https://api-sig.lot.fr/services/GP_AccesInternet/MapServer/1',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560328782-marker-sante.png',
    'type': 'sante',
    'where': 'RMS_OUVERT = "oui"',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{RMS_NOM}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{RMS_ADRESSE}</td></tr><tr><td> </td><td>{RMS_CP} {RMS_VILLE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{RMS_TELEPHONE}</td></tr><tr><td class="icon"><i class="fa fa-envelope"></i></td><td class="info"><a href="mailto: {RMS_MAIL}">Contact par courriel</a></td></tr><tr><td class="icon"><i class="fa fa-calendar"></i></td><td class="info">{RMS_HORAIRES}</td></tr></table>'
  },
  {
    'id': '3',
    'titre': 'maison de santé',
    'url': 'https://api-sig.lot.fr/services/GP_Sante/MapServer/4',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560328782-marker-sante.png',
    'type': 'sante',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{EMS_NOM}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{EMS_ADRESSE}</td></tr><tr><td> </td><td>{EMS_CP} {EMS_COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{EMS_TEL}</td></tr></table>'
  },
  {
    'id': '4',
    'titre': 'centre de planification',
    'url': 'https://api-sig.lot.fr/services/GP_Sante/MapServer/10',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560328782-marker-sante.png',
    'type': 'sante',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{CPE_LIEU}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{CPE_ADRESSE}</td></tr><tr><td> </td><td>{CPE_CP} {CPE_COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{CPE_TEL}</td></tr><tr><td class="icon"><i class="fa fa-envelope"></i></td><td class="info"><a href="mailto: {CPE_EMAIL}">Contact par courriel</a></td></tr><tr><td class="icon"><i class="fa fa-calendar"></i></td><td class="info">{CPE_PERMANENCE}</td></tr></table>'
  },
  {
    'id': '5',
    'titre': 'hopitaux/clinique',
    'url': 'https://api-sig.lot.fr/services/GP_Sante/MapServer/6',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560328782-marker-sante.png',
    'type': 'sante',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{EHO_TOPONYME}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{EHO_ADRESSE}</td></tr><tr><td> </td><td>{EHO_CP} {EHO_COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{EHO_TELEPHONE}</td></tr><tr class="space"><td class="icon"><i class="fa fa-info-circle"></i></td><td class="info"><span>Type de soins : </span>{EHO_TYPEETAB}</td></tr></table>'
  },
  {
    'id': '6',
    'titre': 'pharmacie',
    'url': 'https://api-sig.lot.fr//services/GP_Sante/MapServer/8',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560328782-marker-sante.png',
    'type': 'sante',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{NOM}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{ADRESSE}</td></tr><tr><td> </td><td>{CP} {COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td>{TELEPHONE}</td></tr></table>'
  },
  {
    'id': '7',
    'titre': 'laboratoire d\'analyses médicales',
    'url': 'https://api-sig.lot.fr//services/GP_Sante/MapServer/5',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560328782-marker-sante.png',
    'type': 'sante',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{NOM}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{ADRESSE}</td></tr><tr><td> </td><td>{CP} {COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td>{TELEPHONE}</td></tr></table>'
  },
  {
    'id': '8',
    'titre': 'radiologie',
    'url': 'https://api-sig.lot.fr/services/GP_Sante/MapServer/3',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560328782-marker-sante.png',
    'type': 'sante',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{ERA_NOM}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{ERA_ADRESSE}</td></tr><tr><td> </td><td>{ERA_CP} {ERA_COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{ERA_TELEPHONE}</td></tr></table>'
  },
  {
    'id': '9',
    'titre': 'CPAM',
    'url': 'https://api-sig.lot.fr/services/GP_Social/MapServer/5',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560328782-marker-sante.png',
    'type': 'sante',
    'where': 'SSO_TYPEETAB ="CPAM"',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{SSO_NOM}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{SSO_ADRESSE}</td></tr><tr><td> </td><td>{SSO_CP} {SSO_COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{SSO_TEL}</td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info"><a href="{SSO_SITEWEB}">En savoir plus</a></td></tr></table>'
  },
  {
    'id': '10',
    'titre': 'mutualité sociale agricole',
    'url': 'https://api-sig.lot.fr/services/GP_Social/MapServer/5',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560328782-marker-sante.png',
    'type': 'sante',
    'where': 'SSO_TYPEETAB ="MSA"',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{SSO_NOM}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{SSO_ADRESSE}</td></tr><tr><td> </td><td>{SSO_CP} {SSO_COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{SSO_TEL}</td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info"><a href="{SSO_SITEWEB}">En savoir plus</a></td></tr></table>'
  },

  // enfance/famille
  {
    'id': '11',
    'titre': 'maison des solidarités départementales',
    'url': 'https://api-sig.lot.fr/services/GP_Sante/MapServer/0',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560332411-marker-enfance.png',
    'type': 'enfance',
    'where': 'CMS<>""',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{NOM_DU_SIT}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{ADRESSE}</td></tr><tr><td> </td><td>{CODE_POSTA} {VILLE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{TELMAIL}</td></tr><td class="icon"><i class="fa fa-info"></i></td><td class="info">{INFO_WEB}</td></table>'
  },
  {
    'id': '12',
    'titre': 'consultation PMI',
    'url': 'https://api-sig.lot.fr//services/GP_Sante/MapServer/0',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560332411-marker-enfance.png',
    'type': 'enfance',
    'where': 'PMI<>" "',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{NOM_DU_SIT}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{ADRESSE}</td></tr><tr><td> </td><td>{CODE_POSTA} {VILLE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{TELMAIL}</td></tr></table>'
  },
  {
    'id': '13',
    'titre': 'creche/halte garderies',
    'url': 'https://api-sig.lot.fr/services/GP_Social/MapServer/2',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560332411-marker-enfance.png',
    'type': 'enfance',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{NOM_ETAB}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{ADRESSE}</td></tr><tr><td> </td><td>{CP} {VILLE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{TEL}</td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info"><a href="mailto: {MAIL}">Contact par courriel</a></td></tr><tr><td class="icon"><i class="fa fa-info-circle"></i></td><td class="info">Statut : {TYPEETAB}</td></tr></table>'
  },
  {
    'id': '14',
    'titre': 'assistantes maternelles',
    'url': 'https://api-sig.lot.fr/services/GP_Social/MapServer/15',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560332411-marker-enfance.png',
    'type': 'enfance',
    'where': 'ASM_NB <> "0"',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-child"></i></td><td><b>{ASM_NB}</b> assistante(s) maternelle(s)</td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td> à <b>{ASM_COMMUNE}</b></td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info"><a href="https://lot.fr/faire-garder-son-enfant">Conseils et démarches</a></td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info"><a href="{ASM_SITEWEB}">Trouver une assistante maternelle</a></td></tr></table>'
  },
  {
    'id': '15',
    'titre': 'caisse d\'allocations familliales',
    'url': 'https://api-sig.lot.fr/services/GP_Social/MapServer/5',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560332411-marker-enfance.png',
    'type': 'enfance',
    'where': 'SSO_TYPEETAB ="CAF"',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{SSO_NOM}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{SSO_ADRESSE}</td></tr><tr><td> </td><td>{SSO_CP} {SSO_COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{SSO_TEL}</td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info"><a href="{SSO_SITEWEB}">En savoir plus</a></td></tr></table>'
  },
  {
    'id': '16',
    'titre': 'services protection de l\'enfance',
    'url': 'https://api-sig.lot.fr/services/GP_Social/MapServer/0',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560332411-marker-enfance.png',
    'type': 'enfance',
    'where': 'SITE="CDE" OR SITE="HDD"',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{ANNEXE}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{ADRESSE}</td></tr><tr><td> </td><td>{VILLE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{TELMAIL}</td></tr><tr><td class="icon"><i class="fa fa-envelope"></i></td><td class="info">{EMAIL}</td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info">{INFO_WEB}</td></tr></table>'
  },

  // Séniors
  {
    'id': '17',
    'titre': 'espace personne âgées',
    'url': 'https://api-sig.lot.fr/services/GP_Sante/MapServer/0',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560332502-marker-senior.png',
    'type': 'solidarite',
    'where': 'EPA<>" "',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{ANNEXE}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{ADRESSE}</td></tr><tr><td> </td><td>{VILLE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{TELMAIL}</td></tr><tr><td class="icon"><i class="fa fa-envelope"></i></td><td class="info">{EMAIL}</td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info">{INFO_WEB}</td></tr></table>'
  },
  {
    'id': '18',
    'titre': 'hébergement',
    'url': 'https://api-sig.lot.fr/services/GP_Sante/MapServer/1',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560332502-marker-senior.png',
    'type': 'solidarite',
    'where': 'HEB_CATEGORIE<>"AD"',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info">{HEB_ORGANISME}</td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{HEB_ADRESSE1} {HEB_ADRESSE2}</td></tr><tr><td> </td><td>{HEB_CP} {HEB_VILLE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{HEB_TEL}</td></tr><tr><td class="icon"><i class="fa fa-envelope"></i></td><td class="info"><a href="mailto: {HEB_MAIL}">Contact par courriel</a></td></tr><tr><td class="icon"><i class="fa fa-info-circle"></i></td><td class="info">{HEB_TYPEPRESTA}</td></tr><tr><td class="icon"><i class="fa fa-home"></i></td><td class="info">Capacité autorisée <b>{HEB_CAPACITE_AUTORISEE_ACCUEIL}</b></td></tr><tr><td class="icon"><i class="fa fa-user"></i></td><td class="info">Accueil temporaire <b>{HEB_ACCUEIL_TEMPORAIRE}</b></td></tr></table>'
  },
  {
    'id': '19',
    'titre': 'service d\'aide à domicile',
    'url': 'https://api-sig.lot.fr//services/GP_Sante/MapServer/12',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560332502-marker-senior.png',
    'type': 'solidarite',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info">{AAD_NOM}</td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{AAD_ADRESSE_PERM}</td></tr><tr><td> </td><td>{AAD_CP_PERM} {AAD_COMMUNE_PERM}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{AAD_TELEPHONE}</td></tr><tr><td class="icon"><i class="fa fa-envelope"></i></td><td class="info"><a href="mailto: {AAD_MAIL}">Contact par courriel</a></td></tr><tr><td class="icon"><i class="fa fa-calendar"></i></td><td class="info">{AAD_HORAIRES_PERM}</td></tr></table>'
  },

  // handicap
  {
    'id': '20',
    'titre': 'information et accompagement',
    'url': 'https://api-sig.lot.fr/services/GP_Social/MapServer/4',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560332787-marker-solidarite-2.png',
    'type': 'solidarite',
    'where': 'HEB_TYPE= "SAVS + SAMSAH" or HEB_TYPE= "SAMSAH" or HEB_TYPE= "SAVS"',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{HEB_NOMETAB}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{HEB_ADRESSE1} {HEB_ADRESSE2}</td></tr><tr><td> </td><td>{HEB_CP} {HEB_VILLE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{HEB_TEL}</td></tr><tr><td class="icon"><i class="fa fa-envelope"></i></td><td class="info"><a href="">Contact par courriel</a></td></tr></table>'
  },
  {
    'id': '21',
    'titre': 'hébergement',
    'url': 'https://api-sig.lot.fr/services/GP_Social/MapServer/4',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560332787-marker-solidarite-2.png',
    'type': 'solidarite',
    'where': 'HEB_TYPE= "SAVS + SAMSAH" or HEB_TYPE= "SAMSAH" or HEB_TYPE= "SAVS"',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{HEB_NOMETAB}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{HEB_ADRESSE1}</td></tr><tr><td> </td><td>{HEB_CP} {HEB_VILLE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{HEB_TEL}</td></tr><tr><td class="icon"><i class="fa fa-bed"></i></td><td class="info">Capacité <b>{HEB_CAPACITE_ACCUEIL_AUT}</b> lits</td></tr><tr><td class="icon"><i class="fa fa-home"></i></td><td class="info"><b>{HEB_ACCUEIL_TEMPORAIRE}</b> place(s) pour accueil temporaire </td></tr><tr><td class="icon"><i class="fa fa-stethoscope"></i></td><td class="info"><b>{HEB_MEDICALISE}</b> place(s) médicalisées</td></tr><tr><td class="icon"><i class="fa fa-bed"></i></td><td class="info"> <b>{HEB_VIEILLISSANTE}</b> place(s) pour personnes vieillissantes</td></tr></table>'
  },
  {
    'id': '22',
    'titre': 'centre d\'accueil de jour médicalisés',
    'url': 'https://api-sig.lot.fr/services/GP_Social/MapServer/4',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560332787-marker-solidarite-2.png',
    'type': 'solidarite',
    'where': 'HEB_TYPE="SAT"',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{HEB_NOM}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{HEB_ADRESSE1}</td></tr><tr><td> </td><td>{HEB_CP} {HEB_VILLE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{HEB_TEL}</td></tr><tr><td class="icon"><i class="fa fa-envelope"></i></td><td class="info"><a href="mailto: {HEB_MAIL}">Contact par courriel</a></td></tr></table>'
  },
  {
    'id': '23',
    'titre': 'service d\'aide à domicile',
    'url': 'https://api-sig.lot.fr//services/GP_Sante/MapServer/1',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560332787-marker-solidarite-2.png',
    'type': 'solidarite',
    'where': 'HEB_CATEGORIE="AD"',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{HEB_ORGANISME}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{HEB_ADRESSE1}</td></tr><tr><td> </td><td>{HEB_CP} {HEB_VILLE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{HEB_TEL}</td></tr><tr><td class="icon"><i class="fa fa-envelope"></i></td><td class="info"><a href="{HEB_MAIL}">Contact par courriel</a></td></tr></table>'
  },

  // enseignement
  {
    'id': '24',
    'titre': 'école primaire',
    'url': 'https://api-sig.lot.fr/services/GP_Education/MapServer/0',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333074-marker-education.png',
    'type': 'education',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{EDE_NOM} {EDE_DENOMINATION}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{EDE_ADRESSE}</td></tr><tr><td> </td><td>{EDE_CP} {EDE_COMMUNES}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{EDE_TEL}</td></tr></table>'
  },
  {
    'id': '25',
    'titre': 'collèges',
    'url': 'https://api-sig.lot.fr/services/GP_Education/MapServer/1',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333074-marker-education.png',
    'type': 'education',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{EDE_NOM} {EDE_DENOMINATION}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{EDE_ADRESSE}</td></tr><tr><td> </td><td>{EDE_CP} {EDE_COMMUNES}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{EDE_TEL}</td></tr></table>'
  },
  {
    'id': '26',
    'titre': 'collègue-zone de rattachement',
    'url': 'https://api-sig.lot.fr/services/GP_Education/MapServer/4',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333074-marker-education.png',
    'type': 'education',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info">{NOMCOLLEGE}</td></tr></table>'
  },
  {
    'id': '27',
    'titre': 'lycées',
    'url': 'https://api-sig.lot.fr/services/GP_Education/MapServer/2',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333074-marker-education.png',
    'type': 'education',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{APPELATION}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{ADRESSE}</td></tr><tr><td> </td><td>{CODEPOSTAL} {COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{TELEPHONE}</td></tr><tr><td class="icon"><i class="fa fa-envelope"></i></td><td class="info"><a href="mailto: {COURRIEL}">Contact par courriel</a></td></tr><tr><td class="icon"><i class="fa fa-desktop"></i></td><td class="info"><a href="{SITE_INTERNET}">En savoir plus</a></td></tr></table>'
  },
  {
    'id': '28',
    'titre': 'enseignement supérieur',
    'url': 'https://api-sig.lot.fr/services/GP_Education/MapServer/3',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333074-marker-education.png',
    'type': 'education',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{ESU_APPELATION}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{ESU_ADRESSE}</td></tr><tr><td> </td><td>{ESU_CODEPOSTAL} {ESU_COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{ESU_TELEPHONE}</td></tr><tr><td class="icon"><i class="fa fa-envelope"></i></td><td class="info"><a href="mailto: {ESU_COURRIEL}">Contact par courriel</a></td></tr><tr><td class="icon"><i class="fa fa-graduation-cap"></i></td><td class="info">{ESU_FORMATIONS_BTSDUT}</td></tr><tr><td class="icon"><i class="fa fa-graduation-cap"></i></td><td class="info">{ESU_FORMATION_LICENCESMASTER}</td></tr><tr><td class="icon"><i class="fa fa-graduation-cap"></i></td><td class="info">{ESU_FORMATION_AUTRE}</td></tr></table>'
  },

  // emploi/economie
  {
    'id': '29',
    'titre': 'agences pole emploi',
    'url': 'https://api-sig.lot.fr/services/GP_Eco/MapServer/0',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333191-marker-economie.png',
    'type': 'economie',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{MIL_NOM}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{MIL_ADRESSE}</td></tr><tr><td> </td><td>{MIL_CP} {MIL_COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{MIL_INFO}</td></tr><tr><td class="icon"><i class="fa fa-envelope"></i></td><td class="info"><a href="mailto: {MIL_EMAIL}">Contact par courriel</a></td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info"><a href="{MIL_SITEWEB}">En savoir plus</a></td></tr><tr><td class="icon"><i class="fa fa-calendar"></i></td><td class="info">{MIL_HORAIRES}</td></tr></table>'
  },
  {
    'id': '30',
    'titre': 'mission locales/points info jeunesse',
    'url': 'https://api-sig.lot.fr/services/GP_Eco/MapServer/1',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333191-marker-economie.png',
    'type': 'economie',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{MIL_NOM}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{MIL_ADRESSE}</td></tr><tr><td> </td><td>{MIL_CP} {MIL_COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{MIL_TEL}</td></tr><tr><td class="icon"><i class="fa fa-envelope"></i></td><td class="info"><a href="mailto: {MIL_EMAIL}">Contact par courriel</a></td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info"><a href="{MIL_SITEWEB}">En savoir plus</a></td></tr><tr><td class="icon"><i class="fa fa-calendar"></i></td><td class="info">{MIL_HORAIRES}</td></tr></table>'
  },
  {
    'id': '31',
    'titre': 'zones d\'activité',
    'url': 'https://api-sig.lot.fr/services/GP_Eco/MapServer/4',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333191-marker-economie.png',
    'type': 'economie',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{ZAE_NOM}</b></td></tr><tr><td class="icon"><i class="fa fa-bank"></i></td><td class="info">{ZAE_RESP}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{ZAE_TEL}</td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info"><a href="{ZAE_SITEWEB}">En savoir plus</a></td></tr></table>'
  },

  // commerce/service de proximité
  {
    'id': '32',
    'titre': 'supermarché, superette',
    'url': 'https://api-sig.lot.fr/services/GP_Eco/MapServer/8',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333262-marker-commerce.png',
    'type': 'economie',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info">{ENSEIGNE}</td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{LIBELLE_ADRESSE}</td></tr><tr><td> </td><td>{CP_VILLE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{TELEPHONE}</td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info"><a href="{WEB}">En savoir plus</a></td></tr></table>'
  },
  {
    'id': '33',
    'titre': 'boulangerie, patisserie',
    'url': 'https://api-sig.lot.fr/services/GP_Eco/MapServer/7',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333262-marker-commerce.png',
    'type': 'economie',
    'where': 'MBO_NUMAPE ="1071" or MBO_NUMAPE ="4781"',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info">{MBO_ENSEIGNE}</td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{MBO_ADRESSE}</td></tr><tr><td> </td><td>{MBO_CPVILLE}</td></tr></table>'
  },
  {
    'id': '34',
    'titre': 'boucheries, charcuterie',
    'url': 'https://api-sig.lot.fr/services/GP_Eco/MapServer/7',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333262-marker-commerce.png',
    'type': 'economie',
    'where': 'MBO_NUMAPE="4722" or MBO_NUMAPE="1013"',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info">{MBO_ENSEIGNE}</td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{MBO_ADRESSE}</td></tr><tr><td> </td><td>{MBO_CPVILLE}</td></tr></table>'
  },
  {
    'id': '35',
    'titre': 'agence, bureaux de poste',
    'url': 'https://api-sig.lot.fr/services/GP_Social/MapServer/14',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333262-marker-commerce.png',
    'type': 'economie',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{POS_NOM}</b></td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info">{POS_TYPE}</td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{POS_ADRESSE}</td></tr><tr><td> </td><td>{POS_CP} {POS_COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-map-signs"></i></td><td class="info">{POS_PRECISIONGEO}</td></tr><tr><td class="icon"><i class="fa fa-eur"></i></td><td class="info">Distributeur de billets : {POS_DAB}</td></tr></table>'
  },

  // culture/ patrimoine/ tourisme
  {
    'id': '36',
    'titre': 'musées, centre d\'art',
    'url': 'https://api-sig.lot.fr/services/GP_Culture/MapServer/0',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333514-marker-culture-patrimoine.png',
    'type': 'culture',
    'where': 'ADT_TYPE="Musée"',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{ADT_NOM}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{ADT_ADRESSE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{ADT_TEL}</td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info"><a href="mailto: {ADT_EMAIL}">Contact par courriel</a></td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info"><a href="{ADT_SITEWEB}">En savoir plus</a></td></tr><tr><td class="icon"><i class="fa fa-calendar"></i></td><td class="info">{ADT_HORAIRESL}</td></tr><tr><td class="icon"><i class="fa fa-eur"></i></td><td class="info">{ADT_TARIFSL}</td></tr></table>'
  },
  {
    'id': '37',
    'titre': 'salle de spectacles',
    'url': 'https://api-sig.lot.fr/services/GP_Culture/MapServer/6',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333514-marker-culture-patrimoine.png',
    'type': 'culture',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{SPE_NOMSALLE}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{SPE_ADRESSE}</td></tr><tr><td> </td><td>{SPE_CP} {SPE_COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{SPE_TEL}</td></tr><tr><td class="icon"><i class="fa fa-envelope"></i></td><td class="info"><a href="mailto : {SPE_EMAIL}">Contact par courriel</a></td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info"><a href="{SPE_SITEWEB}">En savoir plus</a></td></tr></table>'
  },
  {
    'id': '38',
    'titre': 'bibliothèques',
    'url': 'https://api-sig.lot.fr/services/GP_Culture/MapServer/4',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333514-marker-culture-patrimoine.png',
    'type': 'culture',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{BDP_NOM}</b></td></tr><tr><td class="icon"><i class="fa fa-signal"></i></td><td class="info"><b>{BDP_NIVEAU}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{BDP_ADRESSE}</td></tr><tr><td> </td><td>{BDP_CP} {BDP_COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{BDP_TELEPHONE}</td></tr><tr><td class="icon"><i class="fa fa-envelope"></i></td><td class="info"><a href="mailto: {BDP_EMAIL}">Contact par courriel</a></td></tr><tr><td class="icon"><i class="fa fa-at"></i></td><td class="info"><a href="{BDP_SITEWEB}">Site internet</a></td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info"><a href="https://bibliotheque.lot.fr">En savoir plus</a></td></tr><tr><td class="icon"><i class="fa fa-calendar"></i></td><td class="info">{BDP_HORAIRESOUVERTURE}</td></tr></table>'
  },
  {
    'id': '39',
    'titre': 'enseignement artistique',
    'url': 'https://api-sig.lot.fr/services/GP_Culture/MapServer/1',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333514-marker-culture-patrimoine.png',
    'type': 'culture',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{EMU_NOM}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{EMU_ADRESSE}</td></tr><tr><td> </td><td>{EMU_CP} {EMU_COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{EMU_TEL}</td></tr><tr><td class="icon"><i class="fa fa-envelope"></i></td><td class="info"><a href="mailto: {EMU_EMAIL}">Contact par courriel</a></td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info"><a href="{EMU_SITEINTERNET}">En savoir plus</a></td></tr><tr><td class="icon"><i class="fa fa-child"></i></td><td class="info">{EMU_NBELEVES} élèves</td></tr></table>'
  },
  {
    'id': '40',
    'titre': 'artothèque',
    'url': 'https://api-sig.lot.fr/services/GP_Culture/MapServer/2',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333514-marker-culture-patrimoine.png',
    'type': 'culture',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{ART_NOM}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{ART_ADRESSE}</td></tr><tr><td> </td><td>{ART_CP} {ART_COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{ART_TEL}</td></tr><tr><td class="icon"><i class="fa fa-envelope"></i></td><td class="info"><a href="mailto:{ART_MAIL}">contact par courriel</a></td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info"><a href="{ART_SITEWEB}">En savoir plus</a></td></tr><tr><td class="icon"><i class="fa fa-calendar"></i></td><td class="info">{ART_HORAIRES}</td></tr></table>'
  },
  {
    'id': '41',
    'titre': 'site patrimoniaux',
    'url': 'https://api-sig.lot.fr/services/GP_Culture/MapServer/0',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333514-marker-culture-patrimoine.png',
    'type': 'culture',
    'where': 'ADT_TYPE = "Site et monument historiques"',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{ADT_NOM}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{ADT_ADRESSE}</td></tr><tr><td> </td><td>{ADT_CP} {ADT_COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{ADT_TEL}</td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info"><a href="mailto: {ADT_EMAIL}">Contact par courriel</a></td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info"><a href="{ADT_SITEWEB}">En savoir plus</a></td></tr><tr><td class="icon"><i class="fa fa-calendar"></i></td><td class="info">{ADT_HORAIRESL}</td></tr></table>'
  },
  {
    'id': '42',
    'titre': 'circuits espaces naturels sensibles',
    'url': 'https://api-sig.lot.fr/services/GP_SportetLoisirs/MapServer/2',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333514-marker-culture-patrimoine.png',
    'type': 'culture',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>Circuit : {EDC_CHEMIN}</b></td></tr><tr><td class="icon"><i class="fa fa-leaf"></i></td><td class="info">Espace naturel sensible : {EDC_ENS}</td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td class="info">Départ : {EDC_NOM}</td></tr><tr><td> </td><td>{EDC_ADRESSE} {EDC_COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info"><a href="{EDC_LIENADT}">En savoir plus</a></td></tr></table>'
  },
  {
    'id': '43',
    'titre': 'archives départementales',
    'url': 'https://api-sig.lot.fr/services/GP_Culture/MapServer/5',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333514-marker-culture-patrimoine.png',
    'type': 'culture',
    'where': 'NOM_DU_SIT = "ARCHIVES DEPARTEMENTALES"',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{NOM_DU_SIT}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{ADRESSE}</td></tr><tr><td> </td><td>{CODE_POSTA} {VILLE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{TELEPHONE}</td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info"><a hrel="{MAIL}">{MAIL}</a></td></tr><tr><td class="icon"><i class="fa fa-calendar"></i></td><td class="info">{HORAIRES}</td></tr></table>'
  },
  {
    'id': '44',
    'titre': 'office de tourrisme',
    'url': 'https://api-sig.lot.fr/services/GP_SportetLoisirs/MapServer/3',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333514-marker-culture-patrimoine.png',
    'type': 'culture',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{OFT_NOM}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{OFT_ADRESSE}</td></tr><tr><td> </td><td>{OFT_CODEPOSTAL} {OFT_COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{OFT_TEL}</td></tr><tr><td class="icon"><i class="fa fa-fax"></i></td><td class="info">{OFT_FAX}</td></tr></table>'
  },

  // accès internet
  {
    'id': '45',
    'titre': 'espaces publics multimédia',
    'url': 'https://api-sig.lot.fr/services/GP_AccesInternet/MapServer/0',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333579-marker-numerique.png',
    'type': 'numerique',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{EPM_NOM}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{EPM_ADRESSE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{EPM_TEL}</td></tr><tr><td class="icon"><i class="fa fa-envelope"></i></td><td class="info"><a href="mailto: {EPM_EMAIL}">Contact par courriel</a></td></tr></table>'
  },
  {
    'id': '46',
    'titre': 'maison de services au public',
    'url': 'https://api-sig.lot.fr/services/GP_AccesInternet/MapServer/1',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333579-marker-numerique.png',
    'type': 'numerique',
    'where': 'RMS_OUVERT = "oui"',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{RMS_NOM}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{RMS_ADRESSE}</td></tr><tr><td> </td><td>{RMS_CP} {RMS_VILLE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{RMS_TELEPHONE}</td></tr><tr><td class="icon"><i class="fa fa-envelope"></i></td><td class="info"><a href="mailto: {RMS_MAIL}">Contact par courriel</a></td></tr><tr><td class="icon"><i class="fa fa-calendar"></i></td><td class="info">{RMS_HORAIRES}</td></tr></table>'
  },
  {
    'id': '47',
    'titre': 'centres de télétravail/coworking',
    'url': 'https://api-sig.lot.fr/services/GP_AccesInternet/MapServer/11',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333579-marker-numerique.png',
    'type': 'numerique',
    'where': 'TLC_NOM<>"LinkFab" and TLC_NOM<>"FunLab" and TLC_NOM<>"FabLab du Grand-Figeac"',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{TLC_NOM}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{TLC_ADRESSE}</td></tr><tr><td> </td><td>{TLC_CP} {TLC_COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{TLC_TEL}</td></tr><tr><td class="icon"><i class="fa fa-envelope"></i></td><td class="info"><a href="mailto: {TLC_EMAIL}">Contact par courriel</a></td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info"><a href="{TLC_SITEWEB}">En savoir plus</a></td></tr><tr><td class="icon"><i class="fa fa-calendar"></i></td><td class="info">{TLC_HORAIRES}</td></tr></table>'
  },
  {
    'id': '48',
    'titre': 'fab labs',
    'url': 'https://api-sig.lot.fr/services/GP_AccesInternet/MapServer/11',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333579-marker-numerique.png',
    'type': 'numerique',
    'where': 'TLC_NOM = "LinkFab" or TLC_NOM="Pôle numérique" or TLC_NOM="FunLab" or TLC_NOM="FabLab du Grand-Figeac"',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{TLC_NOM}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{TLC_ADRESSE}</td></tr><tr><td> </td><td>{TLC_CP} {TLC_COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{TLC_TEL}</td></tr><tr><td class="icon"><i class="fa fa-envelope"></i></td><td class="info"><a href="mailto: {TLC_EMAIL}">Contact par courriel</a></td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info"><a href="{TLC_SITEWEB}">En savoir plus</a></td></tr></table>'
  },
  {
    'id': '49',
    'titre': 'déploiement trés haut débit',
    'url': 'https://api-sig.lot.fr/services/GP_AccesInternet/MapServer/12',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333579-marker-numerique.png',
    'type': 'numerique',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-cog"></i></td><td class="info">Technologie : {THD_TECHNO}</td></tr><tr><td class="icon"><i class="fa fa-rocket"></i></td><td> Débit attendu : {ZONER_DEBIT} mbits/s</td></tr><tr><td class="icon"><i class="fa fa-calendar"></i></td><td class="info">{ZONER_DATE}</td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info"><a href="{THD_LIEN}">En savoir plus</a></td></tr></table>'
  },
  {
    'id': '50',
    'titre': 'déploiement haut débit',
    'url': 'https://api-sig.lot.fr/services/GP_AccesInternet/MapServer/8',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333579-marker-numerique.png',
    'type': 'numerique',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-cog"></i></td><td class="info">Technologie : {MED_TECHNO}</td></tr><tr><td class="icon"><i class="fa fa-rocket"></i></td><td> Débit attendu : {MED_DEBIT} mbits/s</td></tr><tr><td class="icon"><i class="fa fa-calendar"></i></td><td class="info">{MED_DATE}</td></tr></table>'
  },
  {
    'id': '51',
    'titre': 'bornes wifi',
    'url': 'https://api-sig.lot.fr/services/GP_AccesInternet/MapServer/14',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333579-marker-numerique.png',
    'type': 'numerique',
    'where': 'ENSERVICE="Oui"',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info">{OBSERVATION}</td></tr></table>'
  },

  // transport
  {
    'id': '52',
    'titre': 'transport à la demande',
    'url': 'https://api-sig.lot.fr/services/GP_Social/MapServer/13',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333652-marker-transport.png',
    'type': 'transports',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{TAD_NOM} {TAD_STRUCTURES}<b></b></b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td class="info">{TAD_ADRESSE}</td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td class="info">{TAD_CP} {TAD_COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{TAD_TEL}</td></tr><tr><td class="icon"><i class="fa fa-car"></i></td><td class="info">{TAD_NBLIGNES} ligne(s)</td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info"><a href="{TAD_SITEWEB}">En savoir plus</a></td></tr></table>'
  },
  {
    'id': '53',
    'titre': 'lot o\'bus',
    'url': 'https://api-sig.lot.fr/services/GP_Social/MapServer/8',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333652-marker-transport.png',
    'type': 'transports',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{SIT_NOM}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{SIT_ADRESSE}</td></tr><tr><td> </td><td>{SIT_COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{SIT_TELEPHONE}</td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info"><a href="{LOB_LIEN}">En savoir plus</a></td></tr></table>'
  },

  // sécurité
  {
    'id': '54',
    'titre': 'gendaremerie/police nationnale',
    'url': 'https://api-sig.lot.fr/services/GP_Social/MapServer/10',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333708-marker-securite.png',
    'type': 'securite',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{GEN_SERVICE}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{GEN_VOIE}</td></tr><tr><td> </td><td>{GEN_CP} {GEN_COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{GEN_TELEPHONE}</td></tr><tr><td class="icon"><i class="fa fa-envelope"></i></td><td class="info"><a href=" mailto: {GEN_EMAIL}">Contact par courriel</a></td></tr></table>'
  },
  {
    'id': '55',
    'titre': 'caserne de pompiers',
    'url': 'https://api-sig.lot.fr/services/GP_Social/MapServer/1',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333708-marker-securite.png',
    'type': 'securite',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{CSE_NOM}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{CSE_ADRESSE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{CSE_TELEPHONE}</td></tr></table>'
  },

  // environnement
  {
    'id': '56',
    'titre': 'laboratoire d\'analyses départemental',
    'url': 'https://api-sig.lot.fr//services/GP_Sante/MapServer/9',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333782-marker-environnement.png',
    'type': 'environnement',
    'where': 'NOM_DU_SIT ="LABORATOIRE - MAIA-EPA-CAMSP"',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>Laboratoire d analyses départemental</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{ADRESSE}</td></tr><tr><td> </td><td>{CODE_POSTA} {VILLE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{TELEPHONE}</td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info"><a href="{MAIL}">En savoir plus</a></td></tr></table>'
  },
  {
    'id': '57',
    'titre': 'déchèterie/ centre de tri',
    'url': 'value="https://api-sig.lot.fr/services/GP_Social/MapServer/9"',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333782-marker-environnement.png',
    'type': 'environnement',
    'where': 'DEC_TYPESTRUCTURE="Dechetteries"',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{DEC_NOMSITE}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{DEC_ADRESSE}</td></tr><tr><td> </td><td>{DEC_CP} {DEC_COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{DEC_TELEPHONE}</td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info"><a href="{DEC_SITEWEB}">En savoir plus</a></td></tr><tr><td class="icon"><i class="fa fa-calendar"></i></td><td class="info">{DEC_HORAIRES}</td></tr></table>'
  },
  {
    'id': '58',
    'titre': 'syndicat des eaux/assainissement',
    'url': 'https://api-sig.lot.fr/services/GP_Territoiresadm/MapServer/2',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333782-marker-environnement.png',
    'type': 'environnement',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{SIA_NOM}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{SIA_ADRESSE}</td></tr><tr><td> </td><td>{SIA_CP} {SIA_COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{SIA_TEL}</td></tr><tr><td class="icon"><i class="fa fa-envelope"></i></td><td class="info"><a href="mailto:{SIA_EMAIL}">Contact par courriel</a></td></tr><tr><td class="icon"><i class="fa fa-wrench"></i></td><td class="info">{SIA_GESTIONNAIRE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{SIA_TELG}</td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info">{SIA_INFO}</td></tr></table>'
  },

  // service de l'état
  {
    'id': '59',
    'titre': 'trésor public',
    'url': 'https://api-sig.lot.fr/services/GP_Social/MapServer/11',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333833-marker-services-de-letat.png',
    'type': 'eau',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{TPU_NOM}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{TPU_ADRESSE}</td></tr><tr><td> </td><td>{TPU_CP} {TPU_COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{TPU_TEL}</td></tr><tr><td class="icon"><i class="fa fa-envelope"></i></td><td class="info"><a href="mailto:{TPU_MAIL}">Contact par courriel</a></td></tr><tr><td class="icon"><i class="fa fa-calendar"></i></td><td class="info">{TPU_HORAIRES}</td></tr></table>'
  },
  {
    'id': '60',
    'titre': 'prefecture',
    'url': 'https://api-sig.lot.fr/services/GP_Social/MapServer/12',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333833-marker-services-de-letat.png',
    'type': 'eau',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{PRF_NOM}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{PRF_ADRESSE}</td></tr><tr><td> </td><td>{PRF_CP} {PRF_COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{PRF_TEL}</td></tr><tr><td class="icon"><i class="fa fa-envelope"></i></td><td class="info"><a href="mailto: {PRF_MAIL}">Contact par courriel</a></td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info"><a href="{PRF_SITEWEB}">En savoir plus</a></td></tr><tr><td class="icon"><i class="fa fa-calendar"></i></td><td class="info">{PRF_HORAIRES}</td></tr><tr><td class="icon"><i class="fa fa-hand-o-right"></i></td><td class="info">Attention : les guichets des permis de conduire et certificats d’immatriculation de la préfecture du LOT sont définitivement fermés depuis le 31 octobre 2017. Toutes les démarches se font en ligne concernant les permis de conduire et les cartes grises. Des points numériques sont à votre dispositions en préfecture et sous-préfectures.</td></tr></table>'
  },

  // découpage administratif
  {
    'id': '61',
    'titre': 'cantons',
    'url': 'https://api-sig.lot.fr/services/GP_Territoiresadm/MapServer/1',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333924-marker-decoupage.png',
    'type': 'eau',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{NOM}</b></td></tr><tr><td class="icon"><i class="fa fa-briefcase"></i></td><td class="info">Conseillers départementaux : {CONSEILLERS}</td></tr></table>'
  },
  {
    'id': '62',
    'titre': 'intercommunalités',
    'url': 'https://api-sig.lot.fr/services/GP_Territoiresadm/MapServer/4',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333924-marker-decoupage.png',
    'type': 'eau',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{CDC_NOM}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td class="info">{CDC_ADRESSE}</td></tr><tr><td> </td><td>{CDC_CP} {CDC_COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{CDC_TEL}</td></tr><tr><td class="icon"><i class="fa fa-envelope"></i></td><td class="info"><a href="mailto: {CDC_EMAIL}">Contact par courriel</a></td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info"><a href="{CDC_SITEWEB}">En savoir plus</a></td></tr></table>'
  },

  // sport/ activités de pleine nature
  {
    'id': '63',
    'titre': 'comités sprotifs',
    'url': 'https://api-sig.lot.fr/services/GP_SportetLoisirs/MapServer/6',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333988-marker-sport.png',
    'type': 'sport',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info">{SPORT_NOM}</td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{SPORT_ADRESSE}</td></tr><tr><td> </td><td>{SPORT_CP} {SPORT_COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-phone"></i></td><td class="info">{SPORT_TEL}</td></tr><tr><td class="icon"><i class="fa fa-envelope"></i></td><td class="info"><a href="mailto:{SPORT_MAIL}">Contact par courriel</a></td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info"><a href="{SPORT_SITEINTERNET}">En savoir plus</a></td></tr></table>'
  },
  {
    'id': '64',
    'titre': 'baignades surveillées, aménagées',
    'url': 'https://api-sig.lot.fr/services/GP_SportetLoisirs/MapServer/0',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333988-marker-sport.png',
    'type': 'sport',
    'where': 'TYPE_="Plage - baignade, Plage surveillée"',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>{NOM}</b></td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td>{COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td>{TYPE_}</td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td><a href="http://www.tourisme-lot.com/les-incontournables/pleine-nature/se-baigner-en-eau-naturelle/lieux-de-baignade-en-eau-naturelle#!/page/1">En savoir plus</a></td></tr><tr><td class="icon"><i class="fa fa-flask"></i></td><td><a href="{SITEWEB}">Inf eau loisirs</a></td></tr></table>'
  },
  {
    'id': '65',
    'titre': 'circuit espaces naturels sensibles',
    'url': 'https://api-sig.lot.fr/services/GP_SportetLoisirs/MapServer/2',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333988-marker-sport.png',
    'type': 'sport',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-tags"></i></td><td class="info"><b>Circuit : {EDC_CHEMIN}</b></td></tr><tr><td class="icon"><i class="fa fa-leaf"></i></td><td class="info">Espace naturel sensible : {EDC_ENS}</td></tr><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td class="info">Départ : {EDC_NOM}</td></tr><tr><td> </td><td>{EDC_ADRESSE} {EDC_COMMUNE}</td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info"><a href="{EDC_LIENADT}">En savoir plus</a></td></tr></table>'
  },
  {
    'id': '66',
    'titre': 'tourisme fluvial',
    'url': 'https://api-sig.lot.fr/services/GP_SportetLoisirs/MapServer/8',
    'iconUrl': 'https://image.noelshack.com/fichiers/2019/24/3/1560333988-marker-sport.png',
    'type': 'sport',
    'where': '',
    'affichage': '<table class="popup"><tr><td class="icon"><i class="fa fa-map-marker"></i></td><td class="info"><b>{GCM_NAME}</b></td></tr><tr><td class="icon"><i class="fa fa-info"></i></td><td class="info"><a href="{GCM_SITEWEB}">En savoir plus</a></td></tr></table>'
  },
]
export default markers
