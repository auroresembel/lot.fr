// on récupères toutes les dépendances nécessaire pour le fonctionnement du code
import React, { Component } from 'react'
import './CarteStyle.scss'
import * as esri from 'esri-leaflet'
import L from 'leaflet'
import markers from '../marker'
import Select from 'react-select'

import { GestureHandling } from 'leaflet-gesture-handling'

// on déclare les varaibles en globales pour pouvoir les utiliser là où on veux
let values
let lgCommunes = new L.LayerGroup()
let region = ''
let lotLayer
let markerLayer

// fonction permettant de boucler dans le json, de récupérer les données  necesssaires et de les transformer dans un tableaux que react comprend
function parseCommunes (values) {
  return values.map(value => {
    return { label: value.nom, value: value.code }
  })
}

// variable deffinisssant l'esthétique des contours communes
let apiGouvFrGeo = {
  // style de dessin des surfaces
  getStyle: function () {
    let Style = {
      'color': '#3388ff',
  		'weight': 5,
  		'opacity': 1
    }
    return Style
  },
  // supprime le contenu du layer dédié a l'api
  videLayer: function () {
    lgCommunes.clearLayers()
  },
}

class Map extends Component {
  constructor () {
    super()
    // définit les valeur globales du componant (utilisable que dans cette classe)
    this.state = {
      communes: [],
      codecommune: '',
      items: '',
      codeItem: ''
    }
  }

  // on fait une boucle dans le json, on récupèrel'url et inconUrl, on copare les id et les utilises dans la methode permettant d'afficher les markers
  componentWillReceiveProps (nextProps) {
    if (nextProps.dataFromParent) {
      // on otilise la metode .find pour les éléments du json et les utiliser en mettant le nom du tableau et le nom de l'élement
      //  que l'on désire
      let marker = markers.find(element => element.id === nextProps.dataFromParent)
      markerLayer = esri.featureLayer({
        url: marker.url,
        pointToLayer: function (geojson, latlng) {
          return L.marker(latlng, {
            icon: L.icon({
              iconAnchor: [14, 45],
              iconUrl: marker.iconUrl

            })
          })
        },

      })
      // on ajoute à carte
        .addTo(this.map)
      // on met les popup avec les valeurs du markers
        .bindPopup(function (e) {
          let listInfos = e.feature.properties

          return L.Util.template(marker.affichage, listInfos)
        })
    }
  }

  componentDidUpdate () {
    if (this.state.codeItem !== '') {
      if (markerLayer) {
        this.map.removeLayer(markerLayer)
      }
      let marker = markers.find(element => element.id === this.state.codeItem)
      markerLayer = esri.featureLayer({
        url: marker.url,
        where: marker.where,
        spiderfyOnMaxZoom: false,
        disableClusteringAtZoom: 12,
        polygonOptions: {
          color: 'transparent'
        },
        iconCreateFunction: function (cluster) {
          let count = cluster.getChildCount()
          let digits = (count.toString()).length
          return new L.divIcon({
            html: count,
            className: 'cluster digits-' + digits + ' ' + marker.type,
            iconSize: null
          })
        },
        pointToLayer: function (geojson, latlng) {
          return L.marker(latlng, {
            icon: L.icon({
              iconAnchor: [14, 45],
              iconUrl: marker.iconUrl
            })
          })
        },
      }).addTo(this.map).bindPopup(function (e) {
        let listInfos = e.feature.properties

        return L.Util.template(marker.affichage, listInfos)
      })
    }

    // on vérifi si le state.codecommune contient quelque chose et si il contien des données on effectue la methode (fetch)
    if (this.state.codecommune !== '') {
      this.map.removeLayer(lotLayer) // on efface les précédants
      apiGouvFrGeo.videLayer() // on efface les précédants
      // on récupère le json de l'api gouv.fr
      fetch('https://geo.api.gouv.fr/communes/' + this.state.codecommune + '?format=geojson&geometry=contour')
        .then(res => {
          return res.json()
        }).then(json => {
          let geojsonFeature = json
          // puis on définit les le nouveaux layer
          region = L.geoJSON(geojsonFeature, apiGouvFrGeo.getStyle()).addTo(lgCommunes)
          // et on applique le nouveaux layer
          this.map.fitBounds(region.getBounds())
          this.map.addLayer(lgCommunes)
        })
    }
    // si la valeur provenant du parent est égale à "false" on efface le layer
    if (this.props.dataFromParent === false) {
      this.map.removeLayer(markerLayer)
    }
  }

  // on récupère tout les nom des communes du département vie l'api gouv.fr
  getCommunes () {
    fetch('https://geo.api.gouv.fr/departements/46/communes')
      .then(res => {
        return res.json()
      }).then(json => {
        values = json
        this.setState({ communes: parseCommunes(values) })
      })
  }
  // on récupère la valeur du select des nom des communes
  onChangeSelect = event => {
    this.setState({
      codecommune: event.value
    })
  }

  // on récupère la valeur du select des nom des sous-titres
  onChangeItems= event => {
    this.setState({
      codeItem: event.value
    })
  }

  // on boucle dans le json "marker" pour en récupérer les titres du sous-menu, puis on les trasforme en un tazbleau comprehensible
  // pour le bouton select bootstrap
  addItems () {
    let items = markers.map(element => ({
      label: element.titre,
      value: element.id
    }))
    // puis on l'enregistre dans le state locale
    this.setState({
      items: items
    })
  }

  // mettre en route les "fonction"
  componentDidMount () {
    L.Map.addInitHook('addHandler', 'gestureHandling', GestureHandling)
    // on affiche la carte avec point central cahors
    this.map = L.map('map', {
      center: [44.62998207921622, 1.5975630726500185],
      zoom: 8,
      gestureHandling: true,
      layers: [
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
          attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }),
      ]
    })
    // on créée les contours pour le département du lot par defaut
    lotLayer = esri.featureLayer({
      url: 'https://api-sig.lot.fr/services/GP_Territoiresadm/MapServer/6',
      style: function () {
        return {
          color: '#1f26f2',
          weight: 2,
          opacity: 0.5
        }
      }
    }).addTo(this.map)
    // on met en route la fonction du même nom
    this.getCommunes()
    this.addItems()
  }

  render () {
    console.log(this.state.codeItem)
    return (
      <div className="PartieDroite">
        <div className="imputs">
          {/* on affiches les boutons selecte de bootstrap */}
          <div className="form-group col-md-4">
            <div className="drop-down">
              <Select className="selectCommune"
                onChange={this.onChangeSelect}
                options={this.state.communes}
                value={this.state.value}
                clearable={false}
                placeholder="Chercher par nom de communes"
				      />
            </div>
          </div>
          <div className="form-group col-md-4">
            <Select className="selectItems"
              onChange={this.onChangeItems}
              options={this.state.items}
              value={this.state.value}
              clearable={false}
              placeholder="Recherche"
				      />
          </div>
        </div>

        {/* on affiche la carte ici */}
        <div id="map"></div>
      </div>
    )
  }
}

export default Map
